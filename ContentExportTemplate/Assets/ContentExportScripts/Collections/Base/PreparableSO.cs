﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class PreparableSO : ScriptableObject
{
    public abstract void Prepare();

}
