﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

 [CreateAssetMenu(menuName = "Bundles/RefmapBundle")] 
public class RefmapBundle : BundleGeneric<Cubemap>
{
    // v.0.0.2
    // public bool createThumbnails;
    //public Color palettetColor=Color.;
    public override void Prepare()
    {
#if UNITY_EDITOR
        previews = new Texture2D[values.Length];
        if (!Directory.Exists(Path.Combine(Application.dataPath, "GeneratedPreviewImages")))
        {
            Directory.CreateDirectory(Path.Combine(Application.dataPath, "GeneratedPreviewImages"));
        }
        for (int i = 0; i < values.Length; i++)
        {
            var thisTexture = AssetPreview.GetAssetPreview(values[i]);
            string thisname = values[i].name + "_preview.png";
            string path = Path.Combine(Application.dataPath, "GeneratedPreviewImages", thisname);
            File.WriteAllBytes(path, thisTexture.EncodeToPNG());
            //
        }
        AssetDatabase.Refresh();
        for (int i = 0; i < values.Length; i++)
        {
            string thisname = values[i].name + "_preview.png";
            string thispath = "Assets/GeneratedPreviewImages/" + thisname;
            Debug.Log(thispath);
            previews[i] = (Texture2D)AssetDatabase.LoadAssetAtPath(thispath, typeof(Texture2D)); ;
        }
#endif
    }


}
