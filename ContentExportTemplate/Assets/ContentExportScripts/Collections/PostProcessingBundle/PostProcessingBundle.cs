﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.PostProcessing;


[CreateAssetMenu(menuName = "Bundles/PostProcessingProfile Bundle")]
public class PostProcessingBundle : BundleGeneric<PostProcessingProfile>
{
    public override void Prepare()
    {
        previews = new Texture2D[values.Length];
    }

}
