﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BundleGeneric<T> : PreparableSO
{
    public T[] values;
    public Texture2D[] previews;

}
