﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;


//this should be a part of the bundle colleciton but somehow works better as a monobehaviour

public class PrepareHelper : MonoBehaviour
{

    [Header("Click to call Prepare() on object")]
    public PreparableSO preparableObject;
    [ExposeMethodInEditor]
    void Prepare()
    {
        preparableObject.Prepare();
        EditorUtility.SetDirty(preparableObject);
    }

}

#endif