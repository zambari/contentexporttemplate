﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class PreparePostPro : MonoBehaviour
{
    public PostProcessingBundle bundle;

    [ExposeMethodInEditor]
    void RenderPreviews()
    {
        Camera camera = GetComponent<Camera>();
        var beh = camera.GetComponent<PostProcessingBehaviour>();
        RenderTexture preview = new RenderTexture(128, 128, 24);
        RenderTexture savedtarget = camera.targetTexture;
        camera.targetTexture = preview;

        for (int i = 0; i < bundle.values.Length; i++)
        {
            beh.profile = bundle.values[i];
            camera.Render();
            string thisPath = Path.Combine(Application.dataPath, "GeneratedPreviewImages", "postprocessing_" + bundle.values[i].name + "_preview.png");
            preview.DumpToPng(thisPath);
            Debug.Log("rendered " + bundle.values[i].name + " to " + thisPath);
        }
        AssetDatabase.Refresh();
        bundle.previews = new Texture2D[bundle.values.Length];
        for (int i = 0; i < bundle.values.Length; i++)
        {
            string thisname = "postprocessing_" + bundle.values[i].name + "_preview.png";
            string thispath = "Assets/GeneratedPreviewImages/" + thisname;
            Debug.Log(thispath);
            bundle.previews[i] = (Texture2D)AssetDatabase.LoadAssetAtPath(thispath, typeof(Texture2D)); ;
        }
        camera.targetTexture = savedtarget;
    }

}
#endif